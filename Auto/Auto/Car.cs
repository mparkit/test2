﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auto
{
     public class Car
    {
        public string Model { get; set; }
        public string Mark { get; set; }
        public int Year { get; set; }
        public int Price { get; set; }

        public void Show()
        {
            Console.Write($"Car: {Model} {Mark}, {Year}, {Price}");
        }
    }
  
}
