﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auto
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car { Model = "Toyota", Mark = "Celica", Year = 1999, Price = 12000 };
            Car car2 = new Car { Model = "Skoda", Mark = "SuperB", Year = 2015, Price = 27000 };
            car1.Show();
            car2.Show();

            Console.Read();
        }
    }
}
